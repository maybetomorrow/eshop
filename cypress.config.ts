import { defineConfig } from "cypress";

export default defineConfig({
  screenshotOnRunFailure: true,
  defaultCommandTimeout: 35000,
  pageLoadTimeout: 120000,
  video: false,
  retries: 1,
  viewportWidth: 1920,
  viewportHeight: 1080,
  env: {
    environment: "test",
    data: {
      base_url: "url",
    },
  },
  e2e: {
    baseUrl: "url",
  },
});
