export const RubberDuck = {
  Title: "Rubber Duck",
  Description: "Lorem ipsum",
  Price: "5 EUR",
  Url: "/rubber-duck",
};

export const SmallRubberDuck = {
  Title: "Very small rubber Duck",
  Description: "Lorem ipsum",
  Price: "3 EUR",
  Url: "/rubber-duck",
};

export const Customer = {
  Name: "John Doe",
  Number: "123456789",
  Email: "john.doe@example.com",
  PostCode: "12345",
  Address: "Sample address",
};
