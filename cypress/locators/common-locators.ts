export const CommonLocators = {
  search: "#search",
  search_btn: "#search_button",
  cart_btn: "#shopping_cart",
  product_lst: "#product-list",
  product_tit: "#product-title",
  conf_msg: "confirmation-message",
};
