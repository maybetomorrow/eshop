export const CartLocators = {
  cart_list: "#shopping_cart_list",
  qty: "#cart_quantity",
  checkout: "#checkout",
  price: "#cart_price",
  buy: "buy_btn",
};
