export const ItemLocators = {
  title: "#title",
  desc: "#description",
  price: "#price",
  add: "#add_to_cart_button",
};
