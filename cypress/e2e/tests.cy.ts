import {
  CommonLocators,
  CartLocators,
  PurchaseHandler,
  CommonHandler,
  Customer,
  RubberDuck,
  SmallRubberDuck,
} from "../index";

const data = Cypress.env("data");

describe("Eshop tests", () => {
  beforeEach(() => {
    cy.visit(data.base_url);
  });

  it("TC01 - Search item", () => {
    CommonHandler.searchItem("Rubber duck");
  });

  it("TC02 - Show item details", () => {
    CommonHandler.searchItem("Rubber duck");
    cy.get(CommonLocators.product_lst).first().click();
    cy.url().should("contain", "rubber-duck");
    CommonHandler.verifyProductDetail(
      RubberDuck.Title,
      RubberDuck.Description,
      RubberDuck.Price,
    );
  });

  it("TC03 - Add to shopping cart", () => {
    PurchaseHandler.addToCart([RubberDuck.Url]);
    PurchaseHandler.verifyCart([RubberDuck.Title]);
  });

  it("TC04 - Check and update shopping cart", () => {
    PurchaseHandler.addToCart([RubberDuck.Url, SmallRubberDuck.Url]);
    PurchaseHandler.verifyCart([RubberDuck.Title, SmallRubberDuck.Title]);
    PurchaseHandler.changeQuantity(RubberDuck.Price, 3);
  });

  it("TC05 - Buy item", () => {
    PurchaseHandler.addToCart([RubberDuck.Url]);
    PurchaseHandler.verifyCart([RubberDuck.Title]);
    cy.get(CartLocators.checkout).contains("Checkout").click();
    PurchaseHandler.fillForm(Customer);
    cy.get(CartLocators.buy).contains("Buy").click();
    cy.get(CommonLocators.conf_msg).should(
      "contain",
      "Congratulations you have purchased the goods!",
    );
  });
});
