import { CommonLocators, ItemLocators } from "../index";

export class CommonHandler {
  /*
        Searches for an item by name and checks if it is displayed in the search result
    */
  static searchItem(item_name: string) {
    cy.get(CommonLocators.search).type(item_name);
    cy.get(CommonLocators.search_btn).click();
    cy.get(CommonLocators.product_lst).should("not.be.empty");
    cy.get(CommonLocators.product_tit).should("contain", item_name);
  }
  /*
        Checks if the data on the product detail is correctly displayed
    */
  static verifyProductDetail(name: string, desc: string, price: string) {
    cy.get(ItemLocators.title).should("contain", name);
    cy.get(ItemLocators.desc).should("contain", desc);
    cy.get(ItemLocators.price).should("contain", price);
  }
}
