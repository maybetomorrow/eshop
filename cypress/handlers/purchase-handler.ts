import { CommonLocators, ItemLocators, CartLocators } from "../index";

const data = Cypress.env("data");

export class PurchaseHandler {
  /*
        Checks that the list of items in the cart contains the names of the items we have added
    */
  static verifyCart(item_names: string[]) {
    cy.get(CommonLocators.cart_btn).click();
    cy.url().should("contain", data.base_url + "/cart");
    item_names.forEach((item_name) => {
      cy.get(CartLocators.cart_list).should("contain", item_name);
    });
  }

  /*
        Finds and adds the entered items to the cart. In the next version it can be replaced by request
    */
  static addToCart(item_urls: string[]) {
    item_urls.forEach((item_url) => {
      cy.visit(data.base_url + item_url);
      cy.get(ItemLocators.add).click();
      cy.get(CommonLocators.conf_msg).should(
        "contain",
        "Item has been added to your cart!",
      );
    });
  }

  /*
        Changes the quantity of goods in the basket
    */
  static changeQuantity(price: string, qty: number) {
    cy.get(CartLocators.qty).type(String(qty) + "{enter}");
    const newPrice = this.calculatePrice(price, qty);
    cy.get(CartLocators.price).should("contain", newPrice);
  }

  /*
        Calculates the new price of the goods based on the quantity
    */
  static calculatePrice(price: string, qty: number): string {
    let numericPrice = parseFloat(price);
    let totalPrice = numericPrice * qty;
    return totalPrice.toFixed(2) + " EUR";
  }

  static fillForm(customerData: { [key: string]: string }) {
    Object.entries(customerData).forEach(([key, value]) => {
      const selector = `#${key.charAt(0).toLowerCase() + key.slice(1)}`;
      cy.get(selector).type(value);
    });
  }
}
