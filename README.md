# Project Overview

This repository contains a set of Cypress end-to-end tests written in TypeScript, designed to validate the functionality of a fictional e-commerce website. The tests are intended for educational purposes, providing a practical approach to learning and understanding how to automate testing for web applications.

## Getting started

These instructions will guide you through setting up and running the Cypress tests on your local machine for development and testing purposes.

## Prerequisites

Before you can run the tests, you'll need to install the following software:
Node.js: Install Node.js from nodejs.org, which will also install npm (Node Package Manager).
Cypress: Once Node.js is installed, you can install Cypress via npm by running the following command in your terminal:

```
npm install cypress --save-dev
```

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/maybetomorrow/cypress-test.git
git branch -M main
git push -uf origin main
```

## Running tests

```
npx cypress run
```

But it's not possible to run the tests at the moment.

## Project status

This project is for demonstration purposes only.
